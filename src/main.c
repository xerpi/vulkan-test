#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include "linmath.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define ERR_EXIT(...) {fprintf(stderr, __VA_ARGS__); exit(EXIT_FAILURE);}

#define WIDTH 640
#define HEIGHT 480

struct vertex {
	vec3 position;
	vec3 color;
};

struct ubo {
	mat4x4 model;
	mat4x4 view;
	mat4x4 proj;
};

static const char *validation_layers[] = {
	"VK_LAYER_LUNARG_standard_validation"
};

static const char *needed_device_extensions[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
static bool file_load(const char *filename, void **data, uint32_t *size);

static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback_func(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT objectType,
	uint64_t object,
	size_t location,
	int32_t messageCode,
	const char *pLayerPrefix,
	const char *pMessage,
	void *pUserData)
{
	fprintf(stderr, "Debug callback: %s\n", pMessage);
	return VK_FALSE;
}

int main(int argc, char *argv[])
{
	GLFWwindow *window;
	VkResult result;
	VkInstance instance;

	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = glfwCreateWindow(WIDTH, HEIGHT, "vulkan-test", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);

	uint32_t req_extension_count;
	const char **req_extensions = glfwGetRequiredInstanceExtensions(&req_extension_count);

	uint32_t extension_count = req_extension_count + 1;
	const char **extensions;

	extensions = calloc(extension_count, sizeof(*extensions));
	memcpy(extensions, req_extensions, req_extension_count * sizeof(*extensions));
	extensions[req_extension_count] = VK_EXT_DEBUG_REPORT_EXTENSION_NAME;

	VkInstanceCreateInfo create_info = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pNext = NULL,
		.pApplicationInfo = NULL,
		.enabledLayerCount = ARRAY_SIZE(validation_layers),
		.ppEnabledLayerNames = validation_layers,
		.enabledExtensionCount = extension_count,
		.ppEnabledExtensionNames = extensions,
	};

	result = vkCreateInstance(&create_info, NULL, &instance);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateInstance: %d\n", result);

	free(extensions);

	VkDebugReportCallbackCreateInfoEXT debug_callback_info = {
		.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
		.pNext = NULL,
		.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT,
		.pfnCallback = debug_callback_func,
		.pUserData = NULL
	};

	PFN_vkCreateDebugReportCallbackEXT pfn_vkCreateDebugReportCallbackEXT =
		(PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance,
			"vkCreateDebugReportCallbackEXT");

	VkDebugReportCallbackEXT debug_callback;
	pfn_vkCreateDebugReportCallbackEXT(instance, &debug_callback_info, NULL, &debug_callback);

	VkSurfaceKHR surface;
	result = glfwCreateWindowSurface(instance, window, NULL, &surface);
	if (result != VK_SUCCESS)
		ERR_EXIT("glfwCreateWindowSurface: %d\n", result);

	uint32_t physical_device_count;
	vkEnumeratePhysicalDevices(instance, &physical_device_count, NULL);

	VkPhysicalDevice *physical_devices = calloc(physical_device_count,
						    sizeof(*physical_devices));
	vkEnumeratePhysicalDevices(instance, &physical_device_count, physical_devices);

	VkPhysicalDevice picked_physical_device = VK_NULL_HANDLE;
	uint32_t graphics_family_index;
	uint32_t present_family_index;

	for (uint32_t i = 0; i < physical_device_count; i++) {
		bool supports_device_extensions = true;
		bool has_graphics_support = false;
		bool has_present_support = false;

		uint32_t device_extension_count;
		vkEnumerateDeviceExtensionProperties(physical_devices[i], NULL,
						     &device_extension_count, NULL);

		VkExtensionProperties *device_extensions = calloc(device_extension_count,
								  sizeof(*device_extensions));
		vkEnumerateDeviceExtensionProperties(physical_devices[i], NULL,
						     &device_extension_count,
						     device_extensions);

		for (uint32_t j = 0; j < ARRAY_SIZE(needed_device_extensions); j++) {
			bool found = false;

			for (uint32_t k = 0; k < device_extension_count; k++) {
				if (!strcmp(device_extensions[k].extensionName, needed_device_extensions[j])) {
					found = true;
					break;
				}
			}

			if (!found) {
				supports_device_extensions = false;
				break;
			}
		}

		free(device_extensions);

		if (!supports_device_extensions)
			continue;

		uint32_t queue_family_count;
		vkGetPhysicalDeviceQueueFamilyProperties(physical_devices[i],
							 &queue_family_count, NULL);

		VkQueueFamilyProperties *queue_families = calloc(queue_family_count,
								 sizeof(*queue_families));

		vkGetPhysicalDeviceQueueFamilyProperties(physical_devices[i],
							 &queue_family_count,
							 queue_families);

		for (uint32_t j = 0; j < queue_family_count; j++) {
			if (!has_graphics_support &&
			    queue_families[j].queueCount > 0 &&
			    queue_families[j].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				graphics_family_index = j;
				has_graphics_support = true;
			}

			VkBool32 present_support = VK_FALSE;
			vkGetPhysicalDeviceSurfaceSupportKHR(physical_devices[i], j,
							     surface, &present_support);
			if (!has_present_support && present_support) {
				present_family_index = j;
				has_present_support = true;
			}

			if (has_graphics_support && has_present_support)
				break;
		}

		free(queue_families);

		if (has_graphics_support && has_present_support) {
			picked_physical_device = physical_devices[i];
			break;
		}
	}

	free(physical_devices);

	if (picked_physical_device == VK_NULL_HANDLE)
		ERR_EXIT("Can't find a suitable device\n");

	float queue_priorities = 1.0f;
	VkDeviceQueueCreateInfo device_queue_create_info[2] = {
		{
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			.pNext = NULL,
			.flags = 0,
			.queueFamilyIndex = graphics_family_index,
			.queueCount = 1,
			.pQueuePriorities = &queue_priorities,
		},
		{
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			.pNext = NULL,
			.flags = 0,
			.queueFamilyIndex = present_family_index,
			.queueCount = 1,
			.pQueuePriorities = &queue_priorities,
		},
	};

	VkPhysicalDeviceFeatures physical_device_features;
	memset(&physical_device_features, 0, sizeof(physical_device_features));

	uint32_t queue_create_info_count = 1;
	if (graphics_family_index != present_family_index)
		queue_create_info_count = 2;

	VkDeviceCreateInfo device_create_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueCreateInfoCount = queue_create_info_count,
		.pQueueCreateInfos = device_queue_create_info,
		.enabledLayerCount = ARRAY_SIZE(validation_layers),
		.ppEnabledLayerNames = validation_layers,
		.enabledExtensionCount = ARRAY_SIZE(needed_device_extensions),
		.ppEnabledExtensionNames = needed_device_extensions,
		.pEnabledFeatures = &physical_device_features,
	};

	VkDevice device;
	result = vkCreateDevice(picked_physical_device, &device_create_info, NULL, &device);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateDevice: %d\n", result);

	VkQueue graphics_queue;
	vkGetDeviceQueue(device, graphics_family_index, 0, &graphics_queue);

	VkQueue present_queue;
	vkGetDeviceQueue(device, present_family_index, 0, &present_queue);

	uint32_t present_mode_count;
	vkGetPhysicalDeviceSurfacePresentModesKHR(picked_physical_device, surface,
						  &present_mode_count, NULL);

	VkPresentModeKHR *present_modes = calloc(present_mode_count,
						 sizeof(*present_modes));
	vkGetPhysicalDeviceSurfacePresentModesKHR(picked_physical_device, surface,
						  &present_mode_count, NULL);

	VkPresentModeKHR present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
	for (uint32_t i = 0; i < present_mode_count; i++) {
		if (present_modes[i] == VK_PRESENT_MODE_FIFO_KHR) {
			present_mode = VK_PRESENT_MODE_FIFO_KHR;
			break;
		}
	}

	uint32_t surface_format_count;
	vkGetPhysicalDeviceSurfaceFormatsKHR(picked_physical_device, surface,
					     &surface_format_count, NULL);

	VkSurfaceFormatKHR *surface_formats = calloc(surface_format_count,
						 sizeof(*surface_formats));

	vkGetPhysicalDeviceSurfaceFormatsKHR(picked_physical_device, surface,
					     &surface_format_count, surface_formats);

	VkFormat format = VK_FORMAT_UNDEFINED;
	VkColorSpaceKHR colorspace;
	for (int i = 0; i < surface_format_count; i++) {
		if (surface_formats[i].format == VK_FORMAT_R8G8B8A8_SRGB ||
		    surface_formats[i].format == VK_FORMAT_B8G8R8A8_SRGB) {
			format = surface_formats[i].format;
			colorspace = surface_formats[i].colorSpace;
			break;
		}
	}

	if (format == VK_FORMAT_UNDEFINED)
		ERR_EXIT("Can't find a proper surface format\n");

	VkSharingMode image_sharing_mode;
	uint32_t image_sharing_queue_count;
	uint32_t *image_sharing_queues;
	if (graphics_family_index != present_family_index) {
		image_sharing_mode = VK_SHARING_MODE_CONCURRENT;
		image_sharing_queue_count = 2;
		image_sharing_queues = calloc(2, sizeof(*image_sharing_queues));
		image_sharing_queues[0] = graphics_family_index;
		image_sharing_queues[1] = present_family_index;
	} else {
		image_sharing_mode = VK_SHARING_MODE_EXCLUSIVE;
		image_sharing_queue_count = 1;
		image_sharing_queues = calloc(1, sizeof(*image_sharing_queues));
		image_sharing_queues[0] = graphics_family_index;
	}

	VkExtent2D extent = {
		.width = WIDTH,
		.height = HEIGHT,
	};

	VkSwapchainCreateInfoKHR swapchain_create_info = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.pNext = NULL,
		.flags = 0,
		.surface = surface,
		.minImageCount = 2,
		.imageFormat = format,
		.imageColorSpace = colorspace,
		.imageExtent = extent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = image_sharing_mode,
		.queueFamilyIndexCount = image_sharing_queue_count,
		.pQueueFamilyIndices = image_sharing_queues,
		.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = present_mode,
		.clipped = VK_TRUE,
		.oldSwapchain = VK_NULL_HANDLE,
	};

	VkSwapchainKHR swapchain;
	result = vkCreateSwapchainKHR(device, &swapchain_create_info, NULL, &swapchain);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateSwapchainKHR: %d\n", result);

	free(image_sharing_queues);

	uint32_t swapchain_image_count;
	vkGetSwapchainImagesKHR(device, swapchain, &swapchain_image_count, NULL);

	VkImage *swapchain_images = calloc(swapchain_image_count,
					   sizeof(*swapchain_images));
	vkGetSwapchainImagesKHR(device, swapchain, &swapchain_image_count, swapchain_images);

	VkImageView *swapchain_image_views = calloc(swapchain_image_count,
						    sizeof(*swapchain_image_views));
	for (uint32_t i = 0; i < swapchain_image_count; i++) {
		VkImageViewCreateInfo image_view_create_info = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.pNext = NULL,
			.flags = 0,
			.image = swapchain_images[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = format,
			.components = {
				.r = VK_COMPONENT_SWIZZLE_IDENTITY,
				.g = VK_COMPONENT_SWIZZLE_IDENTITY,
				.b = VK_COMPONENT_SWIZZLE_IDENTITY,
				.a = VK_COMPONENT_SWIZZLE_IDENTITY,
			},
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};
		vkCreateImageView(device, &image_view_create_info, NULL,
				  &swapchain_image_views[i]);
	}

	void *vertex_shader_code;
	uint32_t vertex_shader_code_size;
	if (!file_load("shader.vert.spv", &vertex_shader_code, &vertex_shader_code_size))
		ERR_EXIT("Error loading the vertex shader\n");

	void *fragment_shader_code;
	uint32_t fragment_shader_code_size;
	if (!file_load("shader.frag.spv", &fragment_shader_code, &fragment_shader_code_size))
		ERR_EXIT("Error loading the fragment shader\n");

	VkShaderModuleCreateInfo vertex_shader_module_create_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.codeSize = vertex_shader_code_size,
		.pCode = vertex_shader_code,
	};

	VkShaderModule vertex_shader_module;
	result = vkCreateShaderModule(device, &vertex_shader_module_create_info,
				      NULL, &vertex_shader_module);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateShaderModule: %d\n", result);

	VkShaderModuleCreateInfo fragment_shader_module_create_info = {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.codeSize = fragment_shader_code_size,
		.pCode = fragment_shader_code,
	};

	VkShaderModule fragment_shader_module;
	result = vkCreateShaderModule(device, &fragment_shader_module_create_info,
				      NULL, &fragment_shader_module);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateShaderModule: %d\n", result);

	VkPipelineShaderStageCreateInfo vertex_pipeline_shader_stage_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.stage = VK_SHADER_STAGE_VERTEX_BIT,
		.module = vertex_shader_module,
		.pName = "main",
		.pSpecializationInfo = NULL,
	};

	VkPipelineShaderStageCreateInfo fragment_pipeline_shader_stage_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
		.module = fragment_shader_module,
		.pName = "main",
		.pSpecializationInfo = NULL,
	};

	VkPipelineShaderStageCreateInfo pipeline_shader_stages_create_info[] = {
		vertex_pipeline_shader_stage_create_info,
		fragment_pipeline_shader_stage_create_info,
	};

	VkVertexInputBindingDescription vertex_input_binding_description = {
		.binding = 0,
		.stride = sizeof(struct vertex),
		.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
	};

	VkVertexInputAttributeDescription vertex_input_attribute_descriptions[2] = {
		[0] = {
			.location = 0,
			.binding = 0,
			.format = VK_FORMAT_R32G32B32_SFLOAT,
			.offset = offsetof(struct vertex, position),
		},
		[1] = {
			.location = 1,
			.binding = 0,
			.format = VK_FORMAT_R32G32B32_SFLOAT,
			.offset = offsetof(struct vertex, color),
		},
	};

	VkPipelineVertexInputStateCreateInfo pipeline_vertex_input_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.vertexBindingDescriptionCount = 1,
		.pVertexBindingDescriptions = &vertex_input_binding_description,
		.vertexAttributeDescriptionCount = ARRAY_SIZE(vertex_input_attribute_descriptions),
		.pVertexAttributeDescriptions = vertex_input_attribute_descriptions,
	};

	VkPipelineInputAssemblyStateCreateInfo pipeline_input_assembly_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
		.primitiveRestartEnable = VK_FALSE,
	};

	VkViewport viewport = {
		.x = 0.0f,
		.y = 0.0f,
		.width = (float)extent.width,
		.height = (float)extent.height,
		.minDepth = 0.0f,
		.maxDepth = 1.0f,
	};

	VkRect2D scissor = {
		.offset = {
			.x = 0,
			.y = 0,
		},
		.extent = extent,
	};

	VkPipelineViewportStateCreateInfo pipeline_viewport_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.viewportCount = 1,
		.pViewports = &viewport,
		.scissorCount = 1,
		.pScissors = &scissor,
	};

	VkPipelineRasterizationStateCreateInfo pipeline_rasterization_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.depthClampEnable = VK_FALSE,
		.rasterizerDiscardEnable = VK_FALSE,
		.polygonMode = VK_POLYGON_MODE_FILL,
		.cullMode = VK_CULL_MODE_BACK_BIT,
		.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
		.depthBiasEnable = VK_FALSE,
		.depthBiasConstantFactor = 0.0f,
		.depthBiasClamp = 0.0f,
		.depthBiasSlopeFactor = 0.0f,
		.lineWidth = 1.0f,
	};

	VkPipelineMultisampleStateCreateInfo pipeline_multisample_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		.sampleShadingEnable = VK_FALSE,
		.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		.minSampleShading = 1.0f,
		.pSampleMask = NULL,
		.alphaToCoverageEnable = VK_FALSE,
		.alphaToOneEnable = VK_FALSE,
	};

	VkStencilOpState stencilop_state_stub = {
		.failOp = VK_STENCIL_OP_KEEP,
		.passOp = VK_STENCIL_OP_KEEP,
		.depthFailOp = VK_STENCIL_OP_KEEP,
		.compareOp = VK_COMPARE_OP_NEVER,
		.compareMask = 0,
		.writeMask = 0,
		.reference = 0,
	};

	VkPipelineDepthStencilStateCreateInfo pipeline_depthstencil_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.depthTestEnable = VK_TRUE,
		.depthWriteEnable = VK_TRUE,
		.depthCompareOp = VK_COMPARE_OP_LESS,
		.depthBoundsTestEnable = VK_FALSE,
		.stencilTestEnable = VK_FALSE,
		.front = stencilop_state_stub,
		.back = stencilop_state_stub,
		.minDepthBounds = 0.0f,
		.maxDepthBounds = 1.0f,
	};

	VkPipelineColorBlendAttachmentState pipeline_color_blend_attachment_state = {
		.blendEnable = VK_FALSE,
		.srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
		.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
		.colorBlendOp = VK_BLEND_OP_ADD,
		.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
		.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
		.alphaBlendOp = VK_BLEND_OP_ADD,
		.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
				  VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
	};

	VkPipelineColorBlendStateCreateInfo pipeline_color_blend_state_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.logicOpEnable = VK_FALSE,
		.logicOp = VK_LOGIC_OP_COPY,
		.attachmentCount = 1,
		.pAttachments = &pipeline_color_blend_attachment_state,
		.blendConstants[0] = 0.0f,
		.blendConstants[1] = 0.0f,
		.blendConstants[2] = 0.0f,
		.blendConstants[3] = 0.0f,
	};

	VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {
		.binding = 0,
		.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.descriptorCount = 1,
		.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
		.pImmutableSamplers = NULL,
	};

	VkDescriptorSetLayoutCreateInfo descriptor_set_layout_create_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.bindingCount = 1,
		.pBindings = &descriptor_set_layout_binding,
	};

	VkDescriptorSetLayout descriptor_set_layout;
	result = vkCreateDescriptorSetLayout(device, &descriptor_set_layout_create_info,
					     NULL, &descriptor_set_layout);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateDescriptorSetLayout: %d\n", result);

	VkPipelineLayoutCreateInfo pipeline_layout_create_info = {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.setLayoutCount = 1,
		.pSetLayouts = &descriptor_set_layout,
		.pushConstantRangeCount = 0,
		.pPushConstantRanges = NULL,
	};

	VkPipelineLayout pipeline_layout;
	result = vkCreatePipelineLayout(device, &pipeline_layout_create_info,
					NULL, &pipeline_layout);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreatePipelineLayout: %d\n", result);

	VkAttachmentDescription color_attachment_description = {
		.flags = 0,
		.format = format,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
	};

	VkAttachmentDescription depth_attachment_description = {
		.flags = 0,
		.format = VK_FORMAT_D24_UNORM_S8_UINT,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
		.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
		.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
		.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	};

	VkAttachmentReference color_attachment_reference = {
		.attachment = 0,
		.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
	};

	VkAttachmentReference depth_attachment_reference = {
		.attachment = 1,
		.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	};

	VkSubpassDescription subpass_description = {
		.flags = 0,
		.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
		.inputAttachmentCount = 0,
		.pInputAttachments = NULL,
		.colorAttachmentCount = 1,
		.pColorAttachments = &color_attachment_reference,
		.pResolveAttachments = 0,
		.pDepthStencilAttachment = &depth_attachment_reference,
		.preserveAttachmentCount = 0,
		.pPreserveAttachments = NULL,
	};

	VkSubpassDependency subpass_dependency = {
		.srcSubpass = VK_SUBPASS_EXTERNAL,
		.dstSubpass = 0,
		.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		.srcAccessMask = 0,
		.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
				 VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
		.dependencyFlags = 0,
	};

	VkAttachmentDescription renderpass_attachment_description[] = {
		color_attachment_description,
		depth_attachment_description,
	};

	VkRenderPassCreateInfo renderpass_create_info = {
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.attachmentCount = ARRAY_SIZE(renderpass_attachment_description),
		.pAttachments = renderpass_attachment_description,
		.subpassCount = 1,
		.pSubpasses = &subpass_description,
		.dependencyCount = 1,
		.pDependencies = &subpass_dependency,
	};

	VkRenderPass renderpass;
	result = vkCreateRenderPass(device, &renderpass_create_info, NULL,
				    &renderpass);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateRenderPass: %d\n", result);

	VkGraphicsPipelineCreateInfo graphics_pipeline_create_info = {
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.stageCount = ARRAY_SIZE(pipeline_shader_stages_create_info),
		.pStages = pipeline_shader_stages_create_info,
		.pVertexInputState = &pipeline_vertex_input_state_create_info,
		.pInputAssemblyState = &pipeline_input_assembly_state_create_info,
		.pTessellationState = NULL,
		.pViewportState = &pipeline_viewport_state_create_info,
		.pRasterizationState = &pipeline_rasterization_state_create_info,
		.pMultisampleState = &pipeline_multisample_state_create_info,
		.pDepthStencilState = &pipeline_depthstencil_state_create_info,
		.pColorBlendState = &pipeline_color_blend_state_create_info,
		.pDynamicState = NULL,
		.layout = pipeline_layout,
		.renderPass = renderpass,
		.subpass = 0,
		.basePipelineHandle = VK_NULL_HANDLE,
		.basePipelineIndex = -1,
	};

	VkPipeline graphics_pipeline;
	result = vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1,
					   &graphics_pipeline_create_info, NULL,
					   &graphics_pipeline);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateGraphicsPipelines: %d\n", result);

	VkCommandPoolCreateInfo command_pool_create_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.queueFamilyIndex = graphics_family_index,
	};

	VkCommandPool command_pool;
	result = vkCreateCommandPool(device, &command_pool_create_info, NULL,
				     &command_pool);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateCommandPool: %d\n", result);

	VkImageCreateInfo depth_image_create_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = VK_FORMAT_D24_UNORM_S8_UINT,
		.extent = {
			.width = extent.width,
			.height = extent.height,
			.depth = 1,
		},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
		.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	};

	VkImage depth_image;
	result = vkCreateImage(device, &depth_image_create_info, NULL, &depth_image);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateImage: %d\n", result);

	VkMemoryRequirements depth_image_memory_requierements;
	vkGetImageMemoryRequirements(device, depth_image,
				     &depth_image_memory_requierements);


	VkMemoryAllocateInfo depth_image_allocate_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = NULL,
		.allocationSize = depth_image_memory_requierements.size,
		.memoryTypeIndex = 0, /* TODO: Choose proper memory type */
	};

	VkDeviceMemory depth_image_memory;
	result = vkAllocateMemory(device, &depth_image_allocate_info, NULL,
				  &depth_image_memory);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkAllocateMemory: %d\n", result);

	result = vkBindImageMemory(device, depth_image, depth_image_memory, 0);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkBindImageMemory: %d\n", result);

	VkImageViewCreateInfo depth_image_view_create_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.image = depth_image,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.format = VK_FORMAT_D24_UNORM_S8_UINT,
		.components = {
			.r = VK_COMPONENT_SWIZZLE_R,
			.g = VK_COMPONENT_SWIZZLE_G,
			.b = VK_COMPONENT_SWIZZLE_B,
			.a = VK_COMPONENT_SWIZZLE_A,
		},
		.subresourceRange = {
			.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT |
				      VK_IMAGE_ASPECT_STENCIL_BIT,
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
		},
	};

	VkImageView depth_image_view;
	result = vkCreateImageView(device, &depth_image_view_create_info, NULL,
				   &depth_image_view);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateImageView: %d\n", result);

	uint32_t swapchain_framebuffer_count = swapchain_image_count;
	VkFramebuffer *swapchain_framebuffers = calloc(swapchain_framebuffer_count,
						       sizeof(*swapchain_framebuffers));
	for (uint32_t i = 0; i < swapchain_framebuffer_count; i++) {
		VkImageView framebuffer_attachments[] = {
			swapchain_image_views[i],
			depth_image_view,
		};

		VkFramebufferCreateInfo framebuffer_create_info = {
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.pNext = NULL,
			.flags = 0,
			.renderPass = renderpass,
			.attachmentCount = ARRAY_SIZE(framebuffer_attachments),
			.pAttachments = framebuffer_attachments,
			.width = extent.width,
			.height = extent.height,
			.layers = 1,
		};
		vkCreateFramebuffer(device, &framebuffer_create_info, NULL,
				    &swapchain_framebuffers[i]);
	}

	static const vec3 cube_vertices[8] = {
		{-1.0f, -1.0f, -1.0f},
		{-1.0f,  1.0f, -1.0f},
		{ 1.0f, -1.0f, -1.0f},
		{ 1.0f,  1.0f, -1.0f},
		{ 1.0f, -1.0f,  1.0f},
		{ 1.0f,  1.0f,  1.0f},
		{-1.0f, -1.0f,  1.0f},
		{-1.0f,  1.0f,  1.0f},
	};

	static const vec3 cube_face_colors[6] = {
		{1.0f, 0.0f, 0.0f},
		{0.0f, 1.0f, 0.0f},
		{0.0f, 0.0f, 1.0f},
		{1.0f, 1.0f, 0.0f},
		{0.0f, 1.0f, 1.0f},
		{1.0f, 0.0f, 1.0f},
	};

	static const uint16_t cube_face_indices[6][6] = {
		{0, 1, 2, 2, 1, 3},
		{2, 3, 4, 4, 3, 5},
		{4, 5, 6, 6, 5, 7},
		{6, 7, 0, 0, 7, 1},
		{6, 0, 4, 4, 0, 2},
		{1, 7, 3, 3, 7, 5},
	};

	VkBufferCreateInfo vertex_buffer_create_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.size = 3 * 2 * 6 * sizeof(struct vertex),
		.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
	};

	VkBuffer vertex_buffer;
	result = vkCreateBuffer(device, &vertex_buffer_create_info, NULL, &vertex_buffer);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateBuffer: %d\n", result);

	VkMemoryRequirements vertex_buffer_memory_requirements;
	vkGetBufferMemoryRequirements(device, vertex_buffer, &vertex_buffer_memory_requirements);

	VkMemoryAllocateInfo vertex_buffer_memory_allocate_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = NULL,
		.allocationSize = vertex_buffer_memory_requirements.size,
		.memoryTypeIndex = 0, /* TODO: Choose proper memory type */
	};

	VkDeviceMemory vertex_buffer_memory;
	result = vkAllocateMemory(device, &vertex_buffer_memory_allocate_info,
				  NULL, &vertex_buffer_memory);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkAllocateMemory: %d\n", result);

	result = vkBindBufferMemory(device, vertex_buffer, vertex_buffer_memory, 0);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkBindBufferMemory: %d\n", result);

	void *vertex_buffer_memory_mapped;
	vkMapMemory(device, vertex_buffer_memory, 0, 3 * 2 * 6 * sizeof(struct vertex),
		    0, &vertex_buffer_memory_mapped);

	struct vertex *cube_vertex = vertex_buffer_memory_mapped;
	for (uint32_t i = 0; i < ARRAY_SIZE(cube_face_indices); i++) {
		for (uint32_t j = 0; j < ARRAY_SIZE(cube_face_indices[i]); j++) {
			uint32_t idx = 6 * i + j;

			memcpy(&cube_vertex[idx].position,
			       &cube_vertices[cube_face_indices[i][j]],
			       sizeof(cube_vertex[idx].position));

			memcpy(&cube_vertex[idx].color,
			       &cube_face_colors[i],
			       sizeof(cube_vertex[idx].color));
		}
	}

	/* TODO: Flush memory or use a coherent heap */

	vkUnmapMemory(device, vertex_buffer_memory);

	VkBufferCreateInfo uniform_buffer_create_info = {
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.size = sizeof(struct ubo),
		.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.queueFamilyIndexCount = 0,
		.pQueueFamilyIndices = NULL,
	};

	VkBuffer uniform_buffer;
	result = vkCreateBuffer(device, &uniform_buffer_create_info, NULL, &uniform_buffer);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateBuffer: %d\n", result);

	VkMemoryRequirements uniform_buffer_memory_requirements;
	vkGetBufferMemoryRequirements(device, uniform_buffer, &uniform_buffer_memory_requirements);

	VkMemoryAllocateInfo uniform_buffer_memory_allocate_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.pNext = NULL,
		.allocationSize = uniform_buffer_memory_requirements.size,
		.memoryTypeIndex = 0, /* TODO: Choose proper memory type */
	};

	VkDeviceMemory uniform_buffer_memory;
	result = vkAllocateMemory(device, &uniform_buffer_memory_allocate_info,
				  NULL, &uniform_buffer_memory);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkAllocateMemory: %d\n", result);

	result = vkBindBufferMemory(device, uniform_buffer, uniform_buffer_memory, 0);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkBindBufferMemory: %d\n", result);

	VkDescriptorPoolSize descriptor_pool_size = {
		.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.descriptorCount = 1,
	};

	VkDescriptorPoolCreateInfo descriptor_pool_create_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
		.maxSets = 1,
		.poolSizeCount = 1,
		.pPoolSizes = &descriptor_pool_size,
	};

	VkDescriptorPool descriptor_pool;
	result = vkCreateDescriptorPool(device, &descriptor_pool_create_info, NULL,
					&descriptor_pool);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkCreateDescriptorPool: %d\n", result);

	VkDescriptorSetAllocateInfo descriptor_set_allocate_info = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.pNext = NULL,
		.descriptorPool = descriptor_pool,
		.descriptorSetCount = 1,
		.pSetLayouts = &descriptor_set_layout,
	};

	VkDescriptorSet descriptor_set;
	result = vkAllocateDescriptorSets(device, &descriptor_set_allocate_info,
					  &descriptor_set);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkAllocateDescriptorSets: %d\n", result);

	VkDescriptorBufferInfo descriptor_buffer_info = {
		.buffer = uniform_buffer,
		.offset = 0,
		.range = sizeof(struct ubo),
	};

	VkWriteDescriptorSet write_descriptor_set = {
		.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
		.pNext = NULL,
		.dstSet = descriptor_set,
		.dstBinding = 0,
		.dstArrayElement = 0,
		.descriptorCount = 1,
		.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		.pImageInfo = NULL,
		.pBufferInfo = &descriptor_buffer_info,
		.pTexelBufferView = NULL,
	};
	vkUpdateDescriptorSets(device, 1, &write_descriptor_set, 0, NULL);

	uint32_t command_buffer_count = swapchain_framebuffer_count;
	VkCommandBufferAllocateInfo command_buffer_allocate_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.pNext = NULL,
		.commandPool = command_pool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = command_buffer_count,
	};

	VkCommandBuffer *command_buffers = calloc(command_buffer_count,
						  sizeof(*command_buffers));

	result = vkAllocateCommandBuffers(device, &command_buffer_allocate_info,
					  command_buffers);
	if (result != VK_SUCCESS)
		ERR_EXIT("vkAllocateCommandBuffers: %d\n", result);

	for (uint32_t i = 0; i < command_buffer_count; i++) {
		VkCommandBufferBeginInfo command_buffer_begin_info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.pNext = NULL,
			.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
			.pInheritanceInfo = NULL,
		};

		vkBeginCommandBuffer(command_buffers[i], &command_buffer_begin_info);

		VkClearValue renderpass_clear_values[] = {
			{ .color = {
				{0.0f, 0.0f, 0.0f, 1.0f},
			}, },
			{ .depthStencil = {
				.depth = 1.0f,
				.stencil = 0,
			}, },
		};

		VkRenderPassBeginInfo renderpass_begin_info = {
			.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
			.pNext = NULL,
			.renderPass = renderpass,
			.framebuffer = swapchain_framebuffers[i],
			.renderArea = {
				.offset = {
					.x = 0,
					.y = 0,
				},
				.extent = extent,
			},
			.clearValueCount = ARRAY_SIZE(renderpass_clear_values),
			.pClearValues = renderpass_clear_values,
		};

		vkCmdBeginRenderPass(command_buffers[i], &renderpass_begin_info,
				     VK_SUBPASS_CONTENTS_INLINE);

		vkCmdBindPipeline(command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
				  graphics_pipeline);

		vkCmdBindDescriptorSets(command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
					pipeline_layout, 0, 1, &descriptor_set, 0, NULL);

		VkDeviceSize bind_offset = 0;
		vkCmdBindVertexBuffers(command_buffers[i], 0, 1, &vertex_buffer,
				       &bind_offset);

		vkCmdDraw(command_buffers[i], 3 * 2 * 6, 1, 0, 0);

		vkCmdEndRenderPass(command_buffers[i]);

		result = vkEndCommandBuffer(command_buffers[i]);
		if (result != VK_SUCCESS)
			ERR_EXIT("vkEndCommandBuffer: %d\n", result);
	}

	VkSemaphoreCreateInfo image_available_semaphore_create_info = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
	};

	VkSemaphore image_available_semaphore;
	vkCreateSemaphore(device, &image_available_semaphore_create_info, NULL,
			  &image_available_semaphore);

	VkSemaphoreCreateInfo render_finished_semaphore_create_info = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		.pNext = NULL,
		.flags = 0,
	};

	VkSemaphore render_finished_semaphore;
	vkCreateSemaphore(device, &render_finished_semaphore_create_info, NULL,
			  &render_finished_semaphore);

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		static vec3 position = {0.0f, 0.0f, -3.0f};
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			position[2] += 0.001f;
		else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			position[2] -= 0.001f;
		else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			position[0] += 0.001f;
		else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			position[0] -= 0.001f;

		static float rot = 0.0f;
		rot += 0.0002f;

		struct ubo ubo;
		mat4x4_identity(ubo.model);
		mat4x4_rotate_Y(ubo.model, ubo.model, rot);
		mat4x4_rotate_X(ubo.model, ubo.model, rot);
		mat4x4_translate(ubo.view, -position[0], -position[1], -position[2]);
		mat4x4_perspective(ubo.proj, M_PI_2, (float)WIDTH / HEIGHT, 0.01f, 100.0f);

		void *uniform_buffer_memory_mapped;
		vkMapMemory(device, uniform_buffer_memory, 0, sizeof(ubo), 0,
			    &uniform_buffer_memory_mapped);
		memcpy(uniform_buffer_memory_mapped, &ubo, sizeof(ubo));
		vkUnmapMemory(device, uniform_buffer_memory);

		uint32_t image_index;
		vkAcquireNextImageKHR(device, swapchain, UINT64_MAX,
				      image_available_semaphore, VK_NULL_HANDLE,
				      &image_index);

		VkSemaphore submit_semaphore_wait[] = {
			image_available_semaphore,
		};

		VkPipelineStageFlags submit_semaphore_wait_stage[] = {
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		};

		VkSemaphore submit_semaphore_signal[] = {
			render_finished_semaphore,
		};

		VkSubmitInfo submit_info = {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.pNext = NULL,
			.waitSemaphoreCount = ARRAY_SIZE(submit_semaphore_wait),
			.pWaitSemaphores = submit_semaphore_wait,
			.pWaitDstStageMask = submit_semaphore_wait_stage,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffers[image_index],
			.signalSemaphoreCount = ARRAY_SIZE(submit_semaphore_signal),
			.pSignalSemaphores = submit_semaphore_signal,
		};

		result = vkQueueSubmit(graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
		if (result != VK_SUCCESS)
			ERR_EXIT("vkQueueSubmit: %d\n", result);

		VkSemaphore present_semaphore_wait[] = {
			render_finished_semaphore,
		};

		VkPresentInfoKHR present_info = {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.pNext = NULL,
			.waitSemaphoreCount = ARRAY_SIZE(present_semaphore_wait),
			.pWaitSemaphores = present_semaphore_wait,
			.swapchainCount = 1,
			.pSwapchains = &swapchain,
			.pImageIndices = &image_index,
			.pResults = NULL,
		};

		vkQueuePresentKHR(present_queue, &present_info);
	}

	vkDeviceWaitIdle(device);

	vkDestroySemaphore(device, render_finished_semaphore, NULL);
	vkDestroySemaphore(device, image_available_semaphore, NULL);
	vkFreeCommandBuffers(device, command_pool, command_buffer_count, command_buffers);
	vkDestroyDescriptorPool(device, descriptor_pool, NULL);
	vkFreeMemory(device, uniform_buffer_memory, NULL);
	vkDestroyBuffer(device, uniform_buffer, NULL);
	vkFreeMemory(device, vertex_buffer_memory, NULL);
	vkDestroyBuffer(device, vertex_buffer, NULL);
	vkDestroyImageView(device, depth_image_view, NULL);
	vkFreeMemory(device, depth_image_memory, NULL);
	vkDestroyImage(device, depth_image, NULL);
	vkDestroyCommandPool(device, command_pool, NULL);
	for (uint32_t i = 0; i < swapchain_framebuffer_count; i++)
		vkDestroyFramebuffer(device, swapchain_framebuffers[i], NULL);
	vkDestroyPipeline(device, graphics_pipeline, NULL);
	vkDestroyRenderPass(device, renderpass, NULL);
	vkDestroyDescriptorSetLayout(device, descriptor_set_layout, NULL);
	vkDestroyPipelineLayout(device, pipeline_layout, NULL);
	vkDestroyShaderModule(device, fragment_shader_module, NULL);
	vkDestroyShaderModule(device, vertex_shader_module, NULL);
	free(fragment_shader_code);
	free(vertex_shader_code);
	for (uint32_t i = 0; i < swapchain_image_count; i++)
		vkDestroyImageView(device, swapchain_image_views[i], NULL);
	free(swapchain_image_views);
	free(swapchain_images);
	vkDestroySwapchainKHR(device, swapchain, NULL);
	vkDestroyDevice(device, NULL);

	PFN_vkDestroyDebugReportCallbackEXT pfn_vkDestroyDebugReportCallbackEXT =
		(PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance,
			"vkDestroyDebugReportCallbackEXT");

	pfn_vkDestroyDebugReportCallbackEXT(instance, debug_callback, NULL);

	vkDestroySurfaceKHR(instance, surface, NULL);
	vkDestroyInstance(instance, NULL);

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

static bool file_load(const char *filename, void **data, uint32_t *size)
{
	FILE *fp = fopen(filename, "rb");
	if (!fp)
		return false;

	fseek(fp, 0, SEEK_END);
	*size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	*data = malloc(*size);
	fread(*data, *size, 1, fp);

	fclose(fp);

	return true;
}
